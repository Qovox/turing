package machine;

import static java.lang.Math.pow;

public class Metrics {

    /**
     * Operations' number that have been executed
     */
    public static long OPERATIONS;

    /**
     * The execution time of the program, in miliseconds
     */
    public static long EXEC_TIME;

    /**
     * The time when the program started
     */
    private static long startTime;

    public static void startTimer() {

        startTime = System.nanoTime();

    }

    public static void incOp() {
        OPERATIONS++;
    }

    /**
     * Compute metrics' measurement
     */
    public static void measureMetrics() {

        EXEC_TIME = System.nanoTime() - startTime;

    }

    /**
     * Reset all the metrics
     */
    public static void resetMetrics() {

        OPERATIONS = 0;
        EXEC_TIME = 0;

    }

    /**
     * This method displays the metrics after their measurement
     *
     * @return string which displays metrics
     */
    public static String displayMetrics() {

        StringBuilder metrics = new StringBuilder();
        metrics.append("Total operations : ").append(OPERATIONS).append(".\nExecution time : ")
                .append((int) (EXEC_TIME * pow(10, -6))).append(" ms.\n");
        return String.valueOf(metrics);

    }

}
