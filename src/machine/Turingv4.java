package machine;

import exception.BadLength;

public class Turingv4 extends Turing {

    /**
     * Constructor of the Turing machine v2
     * <p>
     * Important variables
     * int wordLength;
     * int head;
     * char[] tape;
     * <p>
     * Additionnal symbols
     * T
     * M
     *
     * @param n word's length
     */
    public Turingv4(int n) throws BadLength {
        super(n);
        if (n % 3 != 0) {
            throw new BadLength(n);
        }
    }

    private void writeT() {
        tape[head] = 'T';
        Metrics.incOp();
    }

    private void writeM() {
        tape[head] = 'M';
        Metrics.incOp();
    }

    boolean isT() {
        return tape[head] == 'T';
    }

    boolean isM() {
        return tape[head] == 'M';
    }

    @Override
    public void run() {
        super.run();
        init();
        if (isX()) {
            initShift();
            if (isX()) {
                do {
                    shift();
                } while (isX());
                writeWord();
            } else
                writeWord();
        } else
            particularCase();
        finalizeRun();
    }

    private void init() {
        right();
        right();
        right();
    }

    private void initShift() {
        left();
        left();
        writeT();
        right();
        right();
        writeT();
        init();
    }

    private void particularCase() {
        left();
        writeB();
        left();
        writeJ();
        left();
        writeR();
    }

    private void shift() {
        writeM();
        while (!isT())
            left();
        do {
            left();
        } while (!isT());
        writeX();
        right();
        writeT();
        do {
            right();
        } while (!isT());
        writeX();
        right();
        right();
        writeT();
        while (!isM())
            right();
        writeX();
        init();
    }

    private void writeWord() {
        left();
        while (!isT()) {
            writeB();
            left();
        }
        if (isT())
            writeJ();
        while (!isT()) {
            writeJ();
            left();
        }
        while (!isBlank()) {
            writeR();
            left();
        }
    }

}
