package machine;

import java.util.concurrent.Callable;

public abstract class Turing {

    private static final String ENDL = ".\n";
    int wordLength;
    private int tapeLength;
    int head;
    char[] tape;

    /**
     * Constructor of the Turing machine v2
     *
     * @param n word's length
     */
    Turing(int n) {
        wordLength = n;
        tapeLength = 3 * wordLength;
        head = 0;
        initTape();
    }

    protected void run() {
        Metrics.resetMetrics();
        Metrics.startTimer();
        head = wordLength;
    }

    void finalizeRun() {
        System.out.println(displayInfo());
        Metrics.measureMetrics();
        System.out.println(Metrics.displayMetrics());
        Metrics.resetMetrics();
        System.out.println(displayTape());
    }

    private void initTape() {
        tape = new char[tapeLength];
        while (head < tapeLength) {
            writeBlank();
            right();
        }
        head = wordLength;
        while (head < wordLength * 2) {
            writeX();
            right();
        }
    }

    void left() {
        head--;
        Metrics.incOp();
    }

    void right() {
        head++;
        Metrics.incOp();
    }

    void writeR() {
        tape[head] = 'R';
        Metrics.incOp();
    }

    void writeJ() {
        tape[head] = 'J';
        Metrics.incOp();
    }

    void writeB() {
        tape[head] = 'B';
        Metrics.incOp();
    }

    void writeX() {
        tape[head] = 'X';
        Metrics.incOp();
    }

    void writeBlank() {
        tape[head] = '#';
        Metrics.incOp();
    }

    void writeSym(char sym) {
        tape[head] = sym;
        Metrics.incOp();
    }

    void loop(Callable action, char... cond) throws Exception {
        for (char c : cond) {
            if (tape[head] == c)
                action.call();
        }
    }

    boolean isR() {
        return tape[head] == 'R';
    }

    boolean isJ() {
        return tape[head] == 'J';
    }

    boolean isB() {
        return tape[head] == 'B';
    }

    boolean isX() {
        return tape[head] == 'X';
    }

    boolean isBlank() {
        return tape[head] == '#';
    }

    boolean RorJ() {
        return tape[head] == 'R' || tape[head] == 'J';
    }

    boolean RorB() {
        return tape[head] == 'R' || tape[head] == 'B';
    }

    boolean JorB() {
        return tape[head] == 'J' || tape[head] == 'B';
    }

    private String displayInfo() {
        String info = "Word's length : " + wordLength + ENDL;
        info += "Tape's length : " + tapeLength + ENDL;
        info += "O(n²) = " + (long) (wordLength * wordLength) + ENDL;
        info += "O(nlogn) = " + (long) (wordLength * Math.log(wordLength)) + ENDL;
        return info;
    }

    private String displayTape() {
        StringBuilder tapeContent = new StringBuilder("|");
        for (int i = 0; i < tapeLength; i++) {
            tapeContent.append(tape[i]);
        }
        tapeContent.append("|\n");
        return tapeContent.toString();
    }

}
