package machine;

import exception.BadLength;

/**
 * This turing machine works for a word whose size is a multiple of 3.
 * Its complexity is O(n²).
 */
public class Turingv2 extends Turing {

    /**
     * Constructor of the Turing machine v2
     * <p>
     * Important variables
     * int wordLength;
     * int head;
     * char[] tape;
     *
     * @param n word's length
     */
    public Turingv2(int n) throws BadLength {
        super(n);
        if (n % 3 != 0) {
            throw new BadLength(n);
        }
    }

    @Override
    public void run() {
        super.run();
        init();
        sortB();
        sortJ();
        finalizeRun();
    }

    private void init() {
        while (tape[head] != '#') {
            writeR();
            right();
            writeJ();
            right();
            writeB();
            right();
        }
        left();
    }

    private void sortB() {
        while (isB())
            left();
        if (isJ()) {
            writeB();
            do {
                left();
            } while (RorJ());
            if (isBlank()) {
                do {
                    right();
                } while (RorJ());
                writeJ();
                return;
            }
            if (isB()) {
                writeJ();
                while (RorJ())
                    right();
            }
        }
        if (isR()) {
            writeB();
            do {
                left();
            } while (RorJ());
            if (isBlank()) {
                do {
                    right();
                } while (RorJ());
                writeR();
                return;
            }
            if (isB()) {
                writeR();
                while (RorJ())
                    right();
            }
        }
        sortB();
    }

    private void sortJ() {
        while (isJ())
            left();
        writeJ();
        do {
            left();
        } while (isR());
        if (isBlank()) {
            do {
                right();
            } while (isR());
            writeR();
            return;
        }
        if (isJ()) {
            writeR();
            while (isR())
                right();
        }
        sortJ();
    }

}
