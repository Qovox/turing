package machine;

import exception.BadLength;

/**
 * This turing machine works for a word whose size is a multiple of 3.
 * Its complexity is O(nlogn).
 */
public class Turingv3 extends Turing {

    /**
     * Constructor of the Turing machine v2
     * <p>
     * Important variables
     * int wordLength;
     * int head;
     * char[] tape;
     * <p>
     * Additionnal symbols
     * S
     * a = 0
     * b = 1
     * c = 0
     * d = 1
     *
     * @param n word's length
     */
    public Turingv3(int n) throws BadLength {
        super(n);
        if (n % 3 != 0) {
            throw new BadLength(n);
        }
    }

    private void writeS() {
        tape[head] = 'S';
        Metrics.incOp();
    }

    private void writelcA() {
        tape[head] = 'a';
        Metrics.incOp();
    }

    private void writelcB() {
        tape[head] = 'b';
        Metrics.incOp();
    }

    private void writelcC() {
        tape[head] = 'c';
        Metrics.incOp();
    }

    private void writelcD() {
        tape[head] = 'd';
        Metrics.incOp();
    }

    private boolean isS() {
        return tape[head] == 'S';
    }

    private boolean islcA() {
        return tape[head] == 'a';
    }

    private boolean islcB() {
        return tape[head] == 'b';
    }

    private boolean islcC() {
        return tape[head] == 'c';
    }

    private boolean islcD() {
        return tape[head] == 'd';
    }

    @Override
    public void run() {
        super.run();
        head = (wordLength * 2) - 1;
        count();
        right();
        divisor();
        cleanDiv();
        copy();
        left();
        writeAllR();
        left();
        writeAllJ();
        writeAllB();
        finalizeRun();
    }

    private void count() {
        counter();
        goBackRight();
        if (isX()) {
            count();
        }
    }

    private void counter() {
        writeS();
        do {
            left();
        } while (islcA() || islcB() || isS());
        if (isBlank()) {
            writelcB();
            return;
        }
        if (isX()) {
            do {
                left();
            } while (islcA() || islcB() || isS());
            if (isX())
                counter();
            if (isBlank()) {
                writelcA();
            }
        }
    }

    private void goBackRight() {
        do {
            right();
        } while (islcA() || islcB() || isS() || isX());
        if (isBlank()) {
            do {
                left();
            } while (islcA() || islcB() || isS());
        }
    }

    private void divisor() {
        if (islcA()) {
            while (islcA())
                right();
            if (islcB())
                writelcA();
        } else if (islcB())
            writelcA();
        right();
        if (islcB()) {
            right();
            divisor();
        } else {
            do {
                right();
            } while (islcB());
            if (islcA()) {
                writelcB();
                right();
                if (islcB()) {
                    right();
                    divisor();
                }
            }
        }
    }

    private void cleanDiv() {
        do {
            left();
        } while (islcA() || islcB() || isS());
        if (isBlank()) {
            right();
            while (islcA()) {
                writeBlank();
                right();
            }
        }
    }

    private void copy() {
        while (islcA() || islcB() || islcC() || islcD())
            right();
        if (isS()) {
            left();
            checkCopy();
        }
        if (isBlank()) {
            do {
                right();
            } while (islcA() || islcB());
            if (islcC() || islcD())
                left();
            checkCopy();
        }
    }

    private void checkCopy() {
        if (islcA())
            copylc('c');
        else if (islcB())
            copylc('d');
    }

    private void copylc(char sym) {
        writeSym(sym);
        do {
            left();
        } while (islcA() || islcB());
        if (isBlank()) {
            do {
                left();
            } while (islcC() || islcD());
            writeSym(sym);
            copy();
        }
    }

    private void writeAllR() {
        decrement();
        if (islcC() || islcD() || isBlank())
            writeOneR();
        if (!isR())
            writeAllR();
    }

    /**
     * Complexity : O(4*log(n) + 5)
     */
    private void decrement() {
        if (islcC()) {
            writelcD(); //O(1)
            left(); //O(1)
            while (islcC()) {
                writelcD();
                left();
            } //O(2*log(n))
            writelcC(); //O(1)
            cleanZero(); //O(2*log(n) + 2)
        } else if (islcD()) {
            writelcC(); //O(1)
            cleanZero(); //O(2*log(n) + 2)
        }
    }

    /**
     * Complexity : O(2*log(n) + 2)
     */
    private void cleanZero() {
        left(); //O(1)
        if (isBlank()) {
            do {
                right();
                if (islcC())
                    writeBlank();
            } while (islcC()); //O(2*log(n))
        }
        right(); //O(1)
    }

    /**
     * Complexity : O()
     */
    private void writeOneR() {
        if (islcC() || islcD()) {
            while (islcC() || islcD() || isR() || isBlank())
                right();
            if (isS()) {
                writeR();
            }
            while (islcC() || islcD() || isR())
                left();
            if (isBlank())
                left();
        } else if (isBlank()) {
            while (islcC() || islcD() || isR() || isBlank())
                right();
            if (isS()) {
                writeR();
            }
            while (isR())
                left();
            if (islcC() || islcD())
                right();
        }
    }

    /**
     * Complexity : O((n/3)*(4n/3 + 5*log(n) + 6)) = O(4n²/9 + 2n + 5n*log(n)/3)
     */
    private void writeAllJ() {
        decrement(); //O(4*log(n) + 5)
        if (islcC() || islcD() || isR())
            writeOneJ(); //O(4n/3 + log(n) + 1)
        if (!isBlank())
            writeAllJ();
    }

    /**
     * Complexity : O(4n/3 + log(n) + 1)
     */
    private void writeOneJ() {
        while (islcC() || islcD() || RorJ()) //O(2n/3 + log(n))
            right();
        if (isS())
            writeJ(); //O(1)
        while (RorJ()) //O(2n/3)
            left();
    }

    /**
     * Complexity : O(6n/3 + log(n))
     */
    private void writeAllB() {
        while(RorJ() || isBlank()) //O(2n/3 + log(n))
            right();
        while(isS()) {
            writeB();
            right();
        } //O(4n/3)
    }

}
