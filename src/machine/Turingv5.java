package machine;

import exception.BadLength;

public class Turingv5 extends Turing {

    /**
     * Constructor of the Turing machine v5
     * <p>
     * Important variables
     * int wordLength;
     * int head;
     * char[] tape;
     *
     * @param n word's length
     */
    public Turingv5(int n) throws BadLength {
        super(n);
        if (n % 3 != 0) {
            throw new BadLength(n);
        }
    }

    @Override
    public void run() {
        super.run();
        init();
        if (isX()) {
            goBack();
            firstShift();
            do {
                goBack();
                if (RorJ() || isB())
                    shift();
                else
                    break;
            } while (true);
        }
        finalizeRun();
    }

    private void init() {
        writeR();
        right();
        writeJ();
        right();
        writeB();
        right();
    }

    private void goBack() {
        right();
        if (isX()) {
            do {
                left();
            } while (!isR());
        }
    }

    private void firstShift() {
        right();
        writeR();
        right();
        writeJ();
        right();
        writeJ();
        right();
        writeB();
        right();
        writeB();
    }

    private void shift() {
        right();
        writeR();
        do {
            right();
        } while (isJ());
        writeJ();
        right();
        writeJ();
        do {
            right();
        } while (isB());
        writeB();
        right();
        writeB();
        right();
        writeB();
    }

}
