package exception;

public class BadLength extends Exception {

    public BadLength() {
        this(0);
    }

    /**
     * @param cause - the cause of the argument issue (see above)
     */
    public BadLength(int cause) {
        super("Incorrect length : " + cause + ". It has to be a multiple of 3.");
        System.out.println(this.getMessage());
        System.exit(1);
    }

}
