package exe;

import machine.Turingv5;

public class Main {

    public static void main(String[] args) throws Exception {
        Turingv5 turingv5 = new Turingv5(9);
        turingv5.run();
    }

}
